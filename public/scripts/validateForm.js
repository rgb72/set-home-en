 function setResponseInput(result,selector,message){
    var id = "#"+selector;

	if(message=='clear'){
        $(id+"_group").removeClass('has-error has-feedback');
        $(id+"_glyphicon").removeClass('glyphicon-remove');
        $(id+"_group").removeClass('has-success has-feedback');
        $(id+"_glyphicon").removeClass('glyphicon-ok');
        $(id+"_label").html('');
    }else{
        if(result==true){
            $(id+"_group").removeClass('has-error has-feedback');
            $(id+"_group").addClass('has-success has-feedback');
            $(id+"_glyphicon").removeClass('glyphicon-remove');
            $(id+"_glyphicon").addClass('glyphicon-ok');
            $(id+"_label").html(message);
        }else {
            $(id+"_group").removeClass('has-success has-feedback');
            $(id+"_group").addClass('has-error has-feedback');
            $(id+"_glyphicon").removeClass('glyphicon-ok');
            $(id+"_glyphicon").addClass('glyphicon-remove');
            $(id+"_label").html(message);
        }
    }
}

function andLogic(value1,value2){
    return value1 && value2;
}

function verifyEmail(email){
    var status = false;
    var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    if (email.search(emailRegEx) == -1) {
    }else {
        status = true;
    }
    return status;
}

function checkID(id) {
    if(id.length != 13) return false;
    for(var i=0, sum=0; i < 12; i++)
        sum += parseFloat(id.charAt(i))*(13-i);
    if((11-sum%11)%10!=parseFloat(id.charAt(12)))
       return false;
    return true;
}

function verifyThaiText(text,space){
    var orgi_text;
    if(space){
        orgi_text='.ๅภถุึคตจขชๆไำพะัีรนยบลฃฟหกดเ้่าสวงผปแอิืทมใฝู฿ฎฑธํ๊ณฯญฐฅฤฆฏโฌ็๋ษศซฉฮฺ์ฒฬฦ ';
    }else {
        orgi_text='ๅภถุึคตจขชๆไำพะัีรนยบลฃฟหกดเ้่าสวงผปแอิืทมใฝู฿ฎฑธํ๊ณฯญฐฅฤฆฏโฌ็๋ษศซฉฮฺ์ฒฬฦ';
    }

    var Char_At="";
    var isThai=true;
    for(var i=0;i<text.length;i++){
        Char_At=text.charAt(i);
        if(orgi_text.indexOf(Char_At)==-1){
            isThai=false;
        }
    }
    return isThai;
}

function verifyNumber(text){
    var reg = /^[0-9]+$/;
    return reg.test(text);
}


	function checkAdditionalProfile(field){
        var fieldValue = $('[name='+field+']').val();
        var tag = $('[name='+field+']').prop('tagName');
        var inputType=$('[name='+field+']').attr('type');

        var result;
        var message;

        if(tag == 'INPUT'){
            if(inputType=='radio'){
                var checkRadio = $('input[type="radio"][name="'+field+'"]:checked');
                fieldValue =  checkRadio.val();
                if (typeof fieldValue == "undefined") {
                    result = false;
                }else{
                    result = true;
                }
            }else if(inputType=='text'){
                if(fieldValue==""){
                    result = false;
                }else{
                    result = true;
                }
            }else if(inputType == 'checkbox'){
                var isCheck = $('input[type="checkbox"][name="'+field+'"]').is(':checked');
                if (!isCheck) {
                    result = false;
                }else{
                    result = true;
                }
            }else if(inputType == 'number'){
                var number = $('input[type="number"][name="'+field+'"]');
                var numberResult=true;
                number.each(function(){
                    var _id = $(this).attr("id");
                    var _class = $(this).attr("class");
					  var min = $(this).attr("min");
					  var max = $(this).attr("max");
                    fieldValue = $(this).val();
          var isUnder = parseInt(fieldValue) < parseInt(min);
          var isOver = parseInt(fieldValue) > parseInt(max);
          if (isOver || isUnder || ((typeof fieldValue == "undefined" || fieldValue=='' || ! verifyNumber(fieldValue)) && _class!='otherNumber')
                            || (_class=='otherNumber' && typeof fieldValue != "undefined" && fieldValue!='' && !verifyNumber(fieldValue))) {
                        numberResult = numberResult && false;
                    }else{
                        numberResult = numberResult && true;
                    }
                });
				
                result = numberResult;
            }
        }else if(tag=='SELECT'){
            //dropdown list
            if(fieldValue==""){
                result = false;
        $('.'+field).find($('.select2-selection--single')).css('border', '1px solid #a94442');
        $("."+field).addClass("has-error");
            }else{
                result = true;
        $('.'+field).find($('.select2-selection--single')).css('border', '1px solid #aaa');
        $("."+field).removeClass("has-error");
            }
        }else{
            result = true;
        }

        setResponseInput(result,field,message);
        return result;
    }
	
	function sortOrderAnswer(a, b){
        var indexA = a.indexOf(".");
        var indexB = b.indexOf(".");
        var valueA = a.substr(0,indexA);
        var valueB = b.substr(0, indexB);
        return parseInt(valueA) > parseInt(valueB) ? 1 : -1;
    }

    //For setting order number into hidden text box for submit form
    function setOrderValue(field){
        // field : survey<survey_id>
        var fieldValue = $('[name='+field+']').val();
        var tag = $('[name='+field+']').prop('tagName');
        var inputType=$('[name='+field+']').attr('type');


        if(inputType=='number'){
            var number = $('input[type="number"][name='+field+']');
            var numberResult=true;
            var orderList = [];
            var previewText="";
            var submitText="";

            var runningId = field.substr(field.indexOf("y")+1);

            number.each(function(){
                var numberId = $(this).attr("id");
                var surveyId = numberId.substr(numberId.indexOf("y")+1);
                var _class = $(this).attr("class");
                var fieldValue = $(this).val(); //order
                var textValue = $("label[for='"+numberId+"']").html(); //text
                if (typeof fieldValue == "undefined" || fieldValue=='') {
                    // nothing
                }else{
                    if(_class=='otherNumber'){
                        // select other and get value from textbox
                        var textboxId = "textbox"+surveyId;
                        textValue= $("input[type='text'][id='"+textboxId+"']").val();
                    }
                    // order + text
                    var textLabel = fieldValue+". "+textValue;
                    orderList.push(textLabel);
                }


            });

            orderList.sort(sortOrderAnswer);

            // Change the list on the page
            for(var i = 0, l = orderList.length; i < l; i++) {
                previewText += orderList[i]+"</br>";
                submitText += orderList[i]+"| ";
            }
			$("#survey"+runningId+"_preview").html(previewText);
            $("#number"+runningId).val(submitText);
        }
    }
	
	function setCheckBoxValue(x){
        var id = $(x).attr("id");
        var value = $("input[type='text'][id='"+id+"']").val();
        var textboxId = id.substr(id.indexOf("textbox")+7);
        var surveyId = "survey"+textboxId;
        $("input[type='checkbox'][id='"+surveyId+"']").val(value);
    }

	function setRadioValue(x){
        var id = $(x).attr("id"); // ex. textbox<survey_id>[<i.count>]
        var value = $("input[type='text'][id='"+id+"']").val();
        var textboxId = id.substr(id.indexOf("textbox")+7);
        var surveyId = "survey"+textboxId;
        $("input[type='radio'][id='"+surveyId+"']").val(value);
    }
	
    function enableTextbox(x){
        var id = $(x).attr("id"); //ex. survey<survey_id>[<t.count>]
        var className = $(x).attr("class");
        var tagName = $(x).prop("tagName");
        var inputType =$(x).attr("type");
        var surveyId = id.substr(id.indexOf("y")+1); // ex. <survey_id>[<t.count>]
        var runningId = surveyId.substr(0,surveyId.indexOf("[")); // ex. <survey_id>
        var textboxId = "textbox"+surveyId;
        var textboxRunningId = "textbox"+runningId;
		
        if(tagName=='INPUT' && inputType=='checkbox'){
            x.blur();
            x.focus();
            if ($(x).is(':checked')){
                $("input[type='text'][id='"+textboxId+"']").attr("readonly",false);
            }else{
                $("input[type='text'][id='"+textboxId+"']").attr("readonly",true);
                $("input[type='text'][id='"+textboxId+"']").val('');
                $("input[type='checkbox'][id='"+id+"']").val('');
            }
        }else if(tagName=='INPUT' && inputType=='number'){
            var value= $("input[type='number'][id='"+id+"']").val();
            if(typeof value != "undefined" && value!=''){
                $("input[type='text'][id='"+textboxId+"']").attr("readonly",false);
            }else{
                $("input[type='text'][id='"+textboxId+"']").attr("readonly",true);
                $("input[type='text'][id='"+textboxId+"']").val('');
            }
        }else if(tagName=='INPUT' && inputType=='radio'){
            x.blur();
            x.focus();
            if ($(x).is(':checked') ){
                if($(x).hasClass( "radioWtText" )){
                    $("input[type='text'][class*='radioTextbox'][name='textbox"+runningId+"']").val('');
                    $("input[type='text'][class*='radioTextbox'][name='textbox"+runningId+"']").attr("readonly",true);
                    //enable textbox
                    $("input[type='text'][id='"+textboxId+"']").attr("readonly",false);
                }else{
                    $("input[type='text'][class*='radioTextbox'][name='textbox"+runningId+"']").val('');
                    $("input[type='text'][class*='radioTextbox'][name='textbox"+runningId+"']").attr("readonly",true);
                }
            }
        }

    }

function verifyEnglishText(text,space){
        var english = /^[A-Za-z0-9]*$/;
        var option = "";
        if(space){
            option = " .-";
        }
        var Char_At="";
        var isEnglish=true;
        for(i=0;i<text.length;i++){
            Char_At=text.charAt(i);
            if(!english.test(Char_At)&&option.indexOf(Char_At)==-1){
                isEnglish=false;
            }
        }
        return isEnglish;
    }

 function eventOnClick()
    {
        this.blur();
        this.focus();
    }