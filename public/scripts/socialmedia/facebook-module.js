// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        fetchInformation();
    } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
        /*document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';*/
    } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
        /*document.getElementById('status').innerHTML = 'Please log ' +
            'into Facebook.';*/
    }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '744400045662480',
        cookie     : true,  // enable cookies to allow the server to access
                            // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.8' // use graph api version 2.8
    });

    // Now that we've initialized the JavaScript SDK, we call
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    FB.getLoginStatus(function(response) {
        //statusChangeCallback(response);
    });

};

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function fetchInformation() {
    console.log('Welcome!  Fetching your information.... ');
    var domainis = "https://member.set.or.th/set/";
	
    FB.api('/me?fields=token_for_business,email,first_name,last_name', function(response) {
        var name = response.first_name;
        var lastname = response.last_name;
        var email = response.email;
        var facebook_id = response.token_for_business;
        var current = new Date();
        var timestamp = current.getTime();	
		
        $.get( domainis + "checkFacebookMember.do", {facebookId: facebook_id, em: email, key: timestamp} )
            .done(function( data ) {
                var obj = data;

                if (obj.result=='onlyfb') {
					email = obj.msg;
					$.ajaxSetup({
						crossDomain: true,
						xhrFields: {
							withCredentials: true
						}
					});
                    $.post(domainis + "facebookMember.do", {email: email, key: timestamp})
                        .done(function (result) {
                            var authenObj = result;
                            if(authenObj.result==true){
								window.location.href = domainis + 'memberFirstPage.do';
                            }
                            else {
								window.location.href = domainis + 'alertMessage.do?msg=err&language=th&country=TH';
                            }
                        });
				} else if (obj.result=='done') {
					window.location.href = domainis + 'alertMessage.do?language=th&country=TH';
                } else {
                    var src = domainis + 'registByFacebook.do?language=th&country=TH';
                    src = src + '&facebookId=' + facebook_id;
                    src = src + '&email=' + email;
                    src = src + '&name=' + name;
                    src = src + '&lastname=' + lastname;
                    window.location.href = src;
                }
			});
    });
}

function facebookLogin(){
    FB.login( function(response) {
            checkLoginState();
        }, { scope: 'email,public_profile' }
    )
}