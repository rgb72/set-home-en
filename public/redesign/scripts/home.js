$('a[href="#"]').click(function(e) {
	e.preventDefault();
});

if('ontouchstart' in document.documentElement) {
	$('.shortcut').addClass('touch');
}

$('.touch .shortcut-item > a').click(function(e) {
	e.preventDefault();
	$(this).parent().toggleClass('active');
	$(this).parent().siblings().removeClass('active');
	$(this).parent().siblings().find('.shortcut-sub-item').removeClass('active')
});

$('.touch .shortcut-sub-item > a').click(function(e) {
	e.preventDefault();
	$(this).parent().toggleClass('active');
	$(this).parent().siblings().removeClass('active');
});